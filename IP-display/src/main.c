// The seven-segment indicator is connected to port B and PD4, PD5, PD6 (digit selection).
// The mr30X0 rs232 is connected to UART (PD0, PD1)

#include <ctype.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>

// device ID
#define DEVICE_ID       0x01    // IP-display

// command from the mr30X0
#define CMD_GET_DEVICE_ID       0x01
#define CMD_SHOW_IP_ADDRES      0x10
#define MAX_COMMAND_LENGTH      16
#define END_OF_COMMAND          0xFF
uint8_t command[MAX_COMMAND_LENGTH];
uint8_t commandCnt = 0;
uint8_t commandReceived = 0;

// answer for the mr30X0
#define MAX_ANSWER_LENGTH 8
#define ANSWER_OK       0x00
#define ANSWER_FAIL     0x01
#define END_OF_ANSWER   0xFF
uint8_t answer[MAX_ANSWER_LENGTH];
uint8_t answerCnt = 0;

// ip addres of the mr30X0
#define IP_ADDRES_LENGTH 12
uint8_t ipAddres[IP_ADDRES_LENGTH] = {2, 5, 5, 2, 5, 5, 2, 5, 5, 2, 5, 5};
const uint8_t ipAddresPoint[IP_ADDRES_LENGTH] PROGMEM = {0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF};

// digit codes for the seven-segment indicator
#define SHOW_ALL_SEGMENTS_IDX 16
#define HIDE_ALL_SEGMENTS_IDX 17
const uint8_t codes[18]  PROGMEM = 
    {
        0xC0, // 0
        0xF9, // 1
        0xA4, // 2
        0xB0, // 3
        0x99, // 4
        0x92, // 5 or S
        0x82, // 6
        0xF8, // 7
        0x80, // 8
        0x90, // 9
        0x88, // A
        0x83, // B
        0xC6, // C
        0xA1, // D
        0x86, // E
        0x8E, // F
        0x00, // hide all
        0xFF  // show all
    };


// ISR for USART RX Complete
// Setup rs232:
// 	 stty -F /dev/ttyATH0 cs8 9600 -cstopb -cread -parenb
// stty -F /dev/ttyATH0 cs8 9600 -cstopb cread -parenb ignbrk -brkint icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts -hupcl
// Send message:
//   echo 168 > /dev/ttyATH0
// receive "1", "6", "8", 0x0D, 0x0A
ISR (USART_RX_vect)
{
    uint8_t receivedByte = 0;
    receivedByte = UDR; 
    command[commandCnt] = receivedByte;
    commandCnt++;
    
    if (commandCnt >= MAX_COMMAND_LENGTH)
    {	
        commandCnt = 0;
    }
    
    if (END_OF_COMMAND == receivedByte)
    {
        commandCnt = 0;
        commandReceived = 1;
    }
}

// ISR for USART Data Register Empty
ISR (USART_UDRE_vect)
{
    UDR = answer[answerCnt];
    if (END_OF_ANSWER != answer[answerCnt])
    {
        answerCnt++;
    }
    else
    {
        answerCnt = 0;
        UCSRB &= ~(1 << UDRIE);
    }
}

// display ip addres on the seven-segment indicator
// started by setting up TCCR0
#define COUNTS_TO_SHOW_DIGITS ((F_CPU) / 8 / 0xFF / 4 * 3)  // if timer freq is clk/8 
#define COUNTS_TO_HIDE_DIGITS ((F_CPU) / 8 / 0xFF / 4 * 1)  // if timer freq is clk/8 
uint8_t ipDigitIdx = 0;
uint8_t ipByteCnt = 0;
uint16_t showCnt = 0;
uint8_t showDigits = 1;
ISR (TIMER0_OVF_vect)
{
    if (showDigits)
    {
        // show one byte of the IP
        
        // hide previous digit
        PORTD &= 0x8F;
        // show next digit
        PORTB = pgm_read_byte(&codes[ipAddres[ipByteCnt * 3 + ipDigitIdx]]) &  pgm_read_byte(&ipAddresPoint[ipByteCnt * 3 + ipDigitIdx]);
        PORTD |= 0x10 << ipDigitIdx;
    
        ipDigitIdx++;
        if (ipDigitIdx == 3)
        {
            ipDigitIdx = 0;
        }
        
        showCnt++;
        if (showCnt > COUNTS_TO_SHOW_DIGITS)
        {
            showCnt = 0;
            showDigits = 0;
            // hide byte
            PORTD &= 0x8F;
            PORTB = 0x00;
        }
    }
    else
    {
        showCnt++;
        if (showCnt > COUNTS_TO_HIDE_DIGITS)
        {
            // show the next byte of IP
            showCnt = 0;
            showDigits = 1;
            ipByteCnt++;
            if (ipByteCnt >= 4)
            {
                ipByteCnt = 0;
                // stop showing ip
                TCCR0B &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));
            }
        }
    }
}

void indicateActiveMode(void)
{
    PORTA |= (1 << PORTA1);
}

void indicateInactiveMode(void)
{
    PORTA &= ~(1 << PORTA1);
}

void answerOk(void)
{
    answer[0] = ANSWER_OK;
    answer[1] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
}

void answerFail(void)
{
    answer[0] = ANSWER_FAIL;
    answer[1] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
}

void getDeviceId(void) 
{
    answer[0] = DEVICE_ID;
    answer[1] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
} 

void showIp(void)
{
    if (END_OF_COMMAND == command[13])
    {
        uint8_t commandCnt = 1;
        uint8_t ipAddressCnt = 0;
        for (uint8_t i = 0; i < 4; i++)
        {
            uint8_t initialZeroes = 1;
            for (uint8_t j = 0; j < 3; j++)
            {
                if (0 == command[commandCnt] && initialZeroes && j != 2)
                {
                    ipAddres[ipAddressCnt] = HIDE_ALL_SEGMENTS_IDX;
                }
                else
                {
                    ipAddres[ipAddressCnt] = command[commandCnt];
                    initialZeroes = 0;
                }
                commandCnt++;
                ipAddressCnt++;
            }
        }
        answerOk();

        // show IP
        TCCR0B |= ((0 << CS02) | (1 << CS01) | (0 << CS00));  // starts timer with clk / 8
    }
    else
    {
        answerFail();   
    }
}

int main(void)
{
    DDRB = 0xFF;    // PB* are for segments
    DDRD |= 0x70;   // PD6-4 are for choosing digit
    PORTD &= 0x8F;  // hide all
    PORTB = 0x00;
    
    DDRA |= 0x02;   // PA1 is for activity led
    PORTA &= 0xFD;
    
    // setup UART 
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On
    // USART Transmitter: On
    // USART Mode: Asynchronous
    // USART Baud Rate: 9600
    // PD0 and PD1 are inputs
    DDRD &= ~(1 << DDD0); 
    PORTD &= ~(1 << PORTD0);
    DDRD &= ~(1 << DDD1);
    PORTD &= ~(1 << PORTD1);
    UCSRA=0x02;
    UCSRB=0x18;
    UCSRC=0x86;
    UBRRH=0x00;
    UBRRL=0x0C;
    UCSRB |= (1 << RXCIE);
    
    TIMSK |= (1 << TOIE0);		// enable TIMER0_OVF interupt
    
    sei();	// turn on interrupts
    
    while(1)
    {
        set_sleep_mode(SLEEP_MODE_IDLE);
        sleep_mode();
        
        if (commandReceived)
        {
            indicateActiveMode();
            
            if (command[0] == CMD_GET_DEVICE_ID)
            {
                // get ID - code 0x01
                
                getDeviceId();
            }
            else if (command[0] == CMD_SHOW_IP_ADDRES)
            {
                // show IP - code 0x10
                // command for "192.168.1.4" is {0x10, 1, 9, 2, 1, 6, 8, 0, 0, 1, 0, 0, 4, 0xFF}
                // echo -ne \\x10\\x01\\x09\\x02\\x01\\x06\\x08\\x00\\x00\\x01\\x00\\x00\\x04\\xFF > /dev/ttyATH0
                
                showIp();
            }
            else
            {
                answerFail(); 
            }
 
            commandReceived = 0;
            memset(command, 0, sizeof(command));
            indicateInactiveMode();
        }
        
    }
}