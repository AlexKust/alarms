#include "ds3231.h"
#include "i2cfunc.h"

#define DS3231_ADDRESS (0xD0)
#define DS3231_SECONDS_REGISTER 0x00
#define DS3231_MINUTES_REGISTER 0x01
#define DS3231_HOURS_REGISTER   0x02
#define DS3231_DAY_REGISTER     0x03
#define DS3231_DATE_REGISTER    0x04
#define DS3231_MONTH_REGISTER   0x05
#define DS3231_YEAR_REGISTER    0x06
#define DS3231_CONTROL_REGISTER 0x0E
#define DS3231_STATUS_REGISTER  0x0F
#define DS3231_AGING_REGISTER   0x10
#define DS3231_TEMPERATURE_MSB  0x11
#define DS3231_TEMPERATURE_LSB  0x12

#define TIME_DATA_LENGTH 8

uint8_t ds3231Dec2Bcd(uint8_t val)
{
	return val + 6 * (val / 10);
}

uint8_t ds3231Bcd2Dec(uint8_t val) 
{
	return val - 6 * (val >> 4);
}


int ds3231WriteRegister(const uint8_t regAddres, const uint8_t regData, uint8_t *pErrorCode)
{
    uint8_t status;
    int result;
    
    do
    {
        if (TWI_OK != i2cStart(&status))
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }

        result = i2cSendSLAW(DS3231_ADDRESS, &status);
        if (TWI_OK != result)
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }
    }while(TWI_OK != result);
    
    if (TWI_OK != i2cSendByte(regAddres, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }
    
    if (TWI_OK != i2cSendByte(regData, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }
    
    i2cStop();

    return DS3231_OK;
}

int ds3231Init(uint8_t *pErrorCode)
{
    if (DS3231_OK != ds3231WriteRegister(DS3231_CONTROL_REGISTER, 0x00, pErrorCode))
    {
        return DS3231_ERROR;
    }
    
    return DS3231_OK;
}

int ds3231WriteTime(const time_t* time, uint8_t *pErrorCode)
{
    if (0 == time || 0 == pErrorCode)
    {
        *pErrorCode = 0;
        return DS3231_ERROR;
    }
    // write regs 0x00 - 0x06

    const int startAddres = DS3231_SECONDS_REGISTER;
    uint8_t status;
    int result;

    do
    {
        if (TWI_OK != i2cStart(&status))
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }

        result = i2cSendSLAW(DS3231_ADDRESS, &status);
        if (TWI_OK != result)
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }
    }while(TWI_OK != result);

    if (TWI_OK != i2cSendByte(startAddres, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    if (TWI_OK != i2cSendByte(ds3231Dec2Bcd(time->seconds), &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    if (TWI_OK != i2cSendByte(ds3231Dec2Bcd(time->minutes), &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    // set hours and 24h mode
    if (TWI_OK != i2cSendByte(ds3231Dec2Bcd(time->hours) & 0x3F, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    // DAY register is not used
    if (TWI_OK != i2cSendByte(0, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    if (TWI_OK != i2cSendByte(ds3231Dec2Bcd(time->day), &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    if (TWI_OK != i2cSendByte(ds3231Dec2Bcd(time->month), &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    if (TWI_OK != i2cSendByte(ds3231Dec2Bcd(time->year), &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }

    i2cStop();

    return DS3231_OK;
}

int ds3231ReadTime(time_t* time, uint8_t *pErrorCode)
{
    if (0 == time || 0 == pErrorCode)
    {
        *pErrorCode = 0;
        return DS3231_ERROR;
    }
    // read regs 0x00 - 0x06
   
    uint8_t status;
    int result;
    int startAddres = DS3231_SECONDS_REGISTER;
    uint8_t timeData[TIME_DATA_LENGTH];
    
    do
    {
        if (TWI_OK != i2cStart(&status))
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }

        result = i2cSendSLAW(DS3231_ADDRESS, &status);
        if (TWI_OK != result)
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }
    }while(TWI_OK != result);
    
    if (TWI_OK != i2cSendByte(startAddres, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }
    
    if (TWI_OK != i2cRepeatStart(&status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }    

    if (TWI_OK != i2cSendSLAR(DS3231_ADDRESS, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }
    
    int i;
    for (i = 0; i < 6; i++)
    {
        if (TWI_OK != i2cReceiveByteAck(&timeData[i], &status))
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }
    }
 
    if (TWI_OK != i2cReceiveByteNack(&timeData[i], &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    } 
    
    time->seconds   = ds3231Bcd2Dec(timeData[0]);
    time->minutes   = ds3231Bcd2Dec(timeData[1]);
    time->hours     = ds3231Bcd2Dec(timeData[2]);
    time->day       = ds3231Bcd2Dec(timeData[4]);
    time->month     = ds3231Bcd2Dec(timeData[5]);
    time->year      = ds3231Bcd2Dec(timeData[6]);

    i2cStop();

    return DS3231_OK;
}

int ds3231ReadTemperature(int8_t *pTempInt, uint8_t *pTempFrac, uint8_t *pErrorCode)
{
    // read regs 0x11 - 0x12
    
    uint8_t status;
    int result;
    int startAddres = DS3231_TEMPERATURE_MSB;
    
    do
    {
        if (TWI_OK != i2cStart(&status))
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }

        result = i2cSendSLAW(DS3231_ADDRESS, &status);
        if (TWI_OK != result)
        {
            *pErrorCode = status;
            return DS3231_ERROR;
        }
    }while(TWI_OK != result);
    
    if (TWI_OK != i2cSendByte(startAddres, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }
    
    if (TWI_OK != i2cRepeatStart(&status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }    

    if (TWI_OK != i2cSendSLAR(DS3231_ADDRESS, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }
    
    if (TWI_OK != i2cReceiveByteAck((uint8_t*)pTempInt, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    }
 
    if (TWI_OK != i2cReceiveByteNack(pTempFrac, &status))
    {
        *pErrorCode = status;
        return DS3231_ERROR;
    } 
    
    if (*pTempInt < 0)
    {   
        *pTempFrac = 100 - ((*pTempFrac) >> 6) * 25;
    }
    else
    {
        *pTempFrac = ((*pTempFrac) >> 6) * 25;
    }
    
    i2cStop();

    return DS3231_OK;
}
