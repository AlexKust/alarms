#ifndef __AT24C32_FUNCS__
#define __AT24C32_FUNCS__

#include <stdint.h>

#define AT24C32_OK 		1
#define AT24C32_ERROR 	0
#define AT24C32_ALARM_DATA_BASE     0x20
#define AT24C32_ALARM_DATA_LENGTH   0x20
#define AT24C32_ALARM_ADDRESS(alarmId) (AT24C32_ALARM_DATA_BASE + AT24C32_ALARM_DATA_LENGTH * (alarmId))

int at24c32WriteByte(const uint16_t address, const uint8_t data, uint8_t *pErrorCode);

int at24c32ReadByte(const uint16_t address, uint8_t *pData, uint8_t *pErrorCode);

#endif // __AT24C32_FUNCS__