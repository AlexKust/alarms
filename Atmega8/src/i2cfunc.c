#include "i2cfunc.h"
#include <util/twi.h>

void i2cInit(void)
{
    // setup Fclk to 50kHz (for F_CPU=1000kHz)
    TWBR = 0x02;
    TWSR &= 0xFC;
}

void i2cDeinit(void)
{
    TWCR = 0;
}

int i2cStart(uint8_t * pStatus)
{
    // Send START condition
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    
    // Wait for the START has been transmitted
    while( ! (TWCR & (1 << TWINT)));
    
    *pStatus = (TWSR & 0xF8);
    if(*pStatus != TW_START)
    {
        return TWI_ERROR;
    }
    
    return TWI_OK;
}

int i2cRepeatStart(uint8_t * pStatus)
{
    // Send START condition
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    
    // Wait for the START has been transmitted
    while( ! (TWCR & (1 << TWINT)));
    
    *pStatus = (TWSR & 0xF8);
    if(*pStatus != TW_REP_START)
    {
        return TWI_ERROR;
    }
    
    return TWI_OK;
}

void i2cStop(void)
{
    // Send STOP condition
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
 
    // Wait for the STOP has been transmitted
    while(TWCR & (1<<TWSTO));
}

int i2cSendSLAW(const uint8_t sla, uint8_t * pStatus)
{
    // load SLA+W and start transmission
    TWDR = (sla & 0xFE) + TW_WRITE;
    TWCR = (1 << TWINT) | (1 << TWEN);
    
    // wait for the SLA+W has been transmitted
    while( ! (TWCR & (1 << TWINT)));
    
    *pStatus = (TWSR & 0xF8);
    if (*pStatus != TW_MT_SLA_ACK)
    {
        return TWI_ERROR;
    }
    
    return TWI_OK;
}

int i2cSendSLAR(const uint8_t sla, uint8_t * pStatus)
{
    // load SLA+W and start transmission
    TWDR = (sla & 0xFE) + TW_READ;
    TWCR = (1 << TWINT) | (1 << TWEN);
    
    // wait for the SLA+W has been transmitted
    while( ! (TWCR & (1 << TWINT)));
    
    *pStatus = (TWSR & 0xF8);
    if (*pStatus != TW_MR_SLA_ACK)
    {
        return TWI_ERROR;
    }
    
    return TWI_OK;
}

int i2cSendByte(const uint8_t data, uint8_t * pStatus)
{
    // load data and start transmission
    TWDR = data;
    TWCR = (1 << TWINT) | (1 << TWEN);
 
    // wait for the data has been transmitted
    while( ! (TWCR & (1 << TWINT)));

    *pStatus = (TWSR & 0xF8);
    if (*pStatus != TW_MT_DATA_ACK)
    {
        return TWI_ERROR;
    }
    
    return TWI_OK;
}

int i2cReceiveByteAck(uint8_t * data, uint8_t * pStatus)
{
    // start receiving data
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);

    // wait for the data has been received
    while( ! (TWCR & (1 << TWINT)));

    *pStatus = (TWSR & 0xF8);
    if (*pStatus != TW_MR_DATA_ACK)
    {
        return TWI_ERROR;
    }

    *data = TWDR;

    return TWI_OK;
}

int i2cReceiveByteNack(uint8_t * data, uint8_t * pStatus)
{
    // start receiving data
    TWCR = (1 << TWINT) | (1 << TWEN);
 
    // wait for the data has been received
    while( ! (TWCR & (1 << TWINT)));
 
    *pStatus = (TWSR & 0xF8);
    if (*pStatus != TW_MR_DATA_NACK)
    {
        return TWI_ERROR;
    }
 
    *data = TWDR;
    
    return TWI_OK;
}