#include "at24c32.h"
#include "i2cfunc.h"

#define AT24c32_ADDRESS (0xAE)

int at24c32WriteByte(const uint16_t address, const uint8_t data, uint8_t *pErrorCode)
{
    uint8_t status;
    int result;
    
    do
    {
        if (TWI_OK != i2cStart(&status))
        {
            *pErrorCode = status;
            return AT24C32_ERROR;
        }

        result = i2cSendSLAW(AT24c32_ADDRESS, &status);
        if (TWI_OK != result)
        {
            *pErrorCode = status;
            return AT24C32_ERROR;
        }
    }while(TWI_OK != result);
    
    uint8_t addrHi = address >> 8;
    if (TWI_OK != i2cSendByte(addrHi, &status))
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    }
    
    uint8_t addrLo = address;
    if (TWI_OK != i2cSendByte(addrLo, &status))
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    }
    
    if (TWI_OK != i2cSendByte(data, &status))
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    }
    
    i2cStop();
    
    // acknowledge polling
    uint16_t ackAttempts = 0xFFFF;
    do
    {
        i2cStart(&status);
        result = i2cSendSLAR(AT24c32_ADDRESS, &status);
        ackAttempts--;
    }while((TWI_OK != result) && (ackAttempts != 0));
    
    if (! ackAttempts)
    {
        *pErrorCode = 0xF1;
        return AT24C32_ERROR;
    }
    
    uint8_t dummyByte;
    result = i2cReceiveByteNack(&dummyByte, &status);
    if (TWI_OK != result)
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    } 

    i2cStop();

    return AT24C32_OK;
}

int at24c32ReadByte(const uint16_t address, uint8_t *pData, uint8_t *pErrorCode)
{
    uint8_t status;
    int result;
    
    do
    {
        if (TWI_OK != i2cStart(&status))
        {
            *pErrorCode = status;
            return AT24C32_ERROR;
        }

        result = i2cSendSLAW(AT24c32_ADDRESS, &status);
        if (TWI_OK != result)
        {
            *pErrorCode = status;
            return AT24C32_ERROR;
        }
    }while(TWI_OK != result);
 

    uint8_t addrHi = address >> 8;
    if (TWI_OK != i2cSendByte(addrHi, &status))
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    }
    
    uint8_t addrLo = address;
    if (TWI_OK != i2cSendByte(addrLo, &status))
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    }
 
    if (TWI_OK != i2cRepeatStart(&status))
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    }

    result = i2cSendSLAR(AT24c32_ADDRESS, &status);
    if (TWI_OK != result)
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    }
 
    result = i2cReceiveByteNack(pData, &status);
    if (TWI_OK != result)
    {
        *pErrorCode = status;
        return AT24C32_ERROR;
    } 

    i2cStop();

    return AT24C32_OK;
}