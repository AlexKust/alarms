#ifndef __ALARM_S_TYPES__
#define __ALARM_S_TYPES__

#define TIMEVAL_IS_NOT_INITIALIZED 0xF1

#include <stdint.h>

typedef struct time_s
{
    uint8_t seconds;
    uint8_t minutes;
    uint8_t hours;
    uint8_t day;
    uint8_t month;
    uint8_t year;
}__attribute__((packed)) time_t;

typedef struct alarm_s
{
    time_t  time;
    uint8_t isActivated;
    uint8_t duration;
    uint8_t enabled; // disabled if 0
}alarm_t;

#endif // __ALARM_S_TYPES__