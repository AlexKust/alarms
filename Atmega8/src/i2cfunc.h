#ifndef __I2C_FUNCS__
#define __I2C_FUNCS__

#include <stdint.h>

#define TWI_OK 		1
#define TWI_ERROR 	0

void i2cInit(void);

void i2cDeinit(void);

int i2cStart(uint8_t * pStatus);

int i2cRepeatStart(uint8_t * pStatus);

void i2cStop(void);

int i2cSendSLAW(const uint8_t sla, uint8_t * pStatus);

int i2cSendSLAR(const uint8_t sla, uint8_t * pStatus);

int i2cSendByte(const uint8_t data, uint8_t * pStatus);

int i2cReceiveByteAck(uint8_t * data, uint8_t * pStatus);

int i2cReceiveByteNack(uint8_t * data, uint8_t * pStatus);

#endif // __I2C_FUNCS__