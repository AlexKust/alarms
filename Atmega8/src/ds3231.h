#ifndef __DS3231_FUNCS__
#define __DS3231_FUNCS__

#include "types.h"
#include <stdint.h>

#define DS3231_OK 		1
#define DS3231_ERROR 	0

int ds3231Init(uint8_t *pErrorCode);

int ds3231WriteTime(const time_t* time, uint8_t *pErrorCode);

int ds3231ReadTime(time_t* time, uint8_t *pErrorCode);
    
int ds3231ReadTemperature(int8_t *pTempInt, uint8_t *pTempFrac, uint8_t *pErrorCode);

#endif // __DS3231_FUNCS__