// The seven-segment indicator is connected to port B and PC0, PC1, PC2 (digit selection).
// The mr3040 rs232 is connected to UART (PD0, PD1)
// The optocoupler 4N27 is connected to PD4
// The 3G led of the mr3040 is connected to external int0 (PD2)

#include "types.h"
#include "at24c32.h"
#include "ds3231.h"
#include "i2cfunc.h"
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>

// command from the mr3040
#define CMD_SHOW_IP_ADDRES  0x10
#define CMD_GET_CURRENT_TIME    0x30
#define CMD_SET_CURRENT_TIME    0x31
#define CMD_GET_ALARM_TIME      0x50
#define CMD_SET_ALARM_TIME      0x70
#define CMD_GET_ALARMS_STATUS   0x80
#define CMD_GET_TEMPERATURE 0x90
#define MAX_COMMAND_LENGTH  32
#define END_OF_COMMAND      0xFF
char command[MAX_COMMAND_LENGTH];
int commandCnt = 0;
int commandReceived = 0;

// answer from the mr3040
#define MAX_ANSWER_LENGTH 32
#define ANSWER_OK   0x00
#define ANSWER_FAIL 0x01
#define END_OF_ANSWER 0xFF
uint8_t answer[MAX_ANSWER_LENGTH];
int answerCnt = 0;

// ip addres of the mr3040
#define IP_ADDRES_LENGTH 12
unsigned int ipAddres[IP_ADDRES_LENGTH] = {2, 5, 5, 2, 5, 5, 2, 5, 5, 2, 5, 5};
unsigned int ipAddresPoint[IP_ADDRES_LENGTH] = {0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF};

// digit codes for the seven-segment indicator
#define SHOW_ALL_SEGMENTS_IDX 16
#define HIDE_ALL_SEGMENTS_IDX 17
const unsigned char codes[18] = 
    {
        0xC0, // 0
        0xF9, // 1
        0xA4, // 2
        0xB0, // 3
        0x99, // 4
        0x92, // 5 or S
        0x82, // 6
        0xF8, // 7
        0x80, // 8
        0x90, // 9
        0x88, // A
        0x83, // B
        0xC6, // C
        0xA1, // D
        0x86, // E
        0x8E, // F
        0x00, // hide all
        0xFF  // show all
    };


#define MAX_ALARMS_NUMBER       6

// interrupt service routine for INT0
// To generate execute:
// echo 1 > /sys/class/leds/tp-link\:green\:3g/brightness
// or
// echo 0 > /sys/class/leds/tp-link\:green\:3g/brightness
ISR(INT0_vect)
{
    // do nothing
}	

// ISR for USART RX Complete
// Setup rs232:
// 	 stty -F /dev/ttyATH0 cs8 9600 -cstopb -cread -parenb
// stty -F /dev/ttyATH0 cs8 9600 -cstopb cread -parenb ignbrk -brkint icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts -hupcl
// Send message:
//   echo 168 > /dev/ttyATH0
// receive "1", "6", "8", 0x0D, 0x0A
ISR (USART_RXC_vect)
{
    char receivedByte = 0;
    receivedByte = UDR; 
    command[commandCnt] = receivedByte;
    commandCnt++;
    
    if (commandCnt >= MAX_COMMAND_LENGTH)
    {	
        commandCnt = 0;
    }
    
    if (END_OF_COMMAND == receivedByte)
    {
        commandCnt = 0;
        commandReceived = 1;
    }
}

// ISR for USART Data Register Empty
ISR (USART_UDRE_vect)
{
    UDR = answer[answerCnt];
    if (END_OF_ANSWER != answer[answerCnt])
    {
        answerCnt++;
    }
    else
    {
        answerCnt = 0;
        UCSRB &= ~(1 << UDRIE);
    }
}

// display ip addres on the seven-segment indicator
// started by setting up TCCR0
#define COUNTS_TO_SHOW_DIGITS ((F_CPU) / 8 / 0xFF / 4 * 3)  // if timer freq is clk/8 
#define COUNTS_TO_HIDE_DIGITS ((F_CPU) / 8 / 0xFF / 4 * 1)  // if timer freq is clk/8 
unsigned int ipDigitIdx = 0;
unsigned int ipByteCnt = 0;
unsigned int showCnt = 0;
unsigned int showDigits = 1;
ISR (TIMER0_OVF_vect)
{
    if (showDigits)
    {
        // show one byte of the IP
        
        // hide previous digit
        PORTC &= 0xF8;
        // show next digit
        PORTB = codes[ipAddres[ipByteCnt * 3 + ipDigitIdx]] & ipAddresPoint[ipByteCnt * 3 + ipDigitIdx];
        PORTC |= 0x01 << ipDigitIdx;
    
        ipDigitIdx++;
        if (ipDigitIdx == 3)
        {
            ipDigitIdx = 0;
        }
        
        showCnt++;
        if (showCnt > COUNTS_TO_SHOW_DIGITS)
        {
            showCnt = 0;
            showDigits = 0;
            // hide byte
            PORTC &= 0xF8;
            PORTB = 0x00;
        }
    }
    else
    {
        showCnt++;
        if (showCnt > COUNTS_TO_HIDE_DIGITS)
        {
            // show the next byte of IP
            showCnt = 0;
            showDigits = 1;
            ipByteCnt++;
            if (ipByteCnt >= 4)
            {
                ipByteCnt = 0;
                // stop showing ip
                TCCR0 &= ~(1 << CS01);
            }
        }
    }
}

alarm_t alarms[MAX_ALARMS_NUMBER];

int checkOfAlarmsRequired = 0;
ISR (TIMER1_COMPA_vect)
{
    checkOfAlarmsRequired = 1;
}


void deactivateAlarm(const int id);

#define TMR2_OVERFLOWS_PER_SECOND F_CPU / 256 / 256
uint16_t alarmDurations[MAX_ALARMS_NUMBER];
ISR (TIMER2_OVF_vect)
{
    int allDeactivated = 1;
    int alarmId;
    for (alarmId = 0; alarmId < MAX_ALARMS_NUMBER; alarmId++)
    {
        if (alarmDurations[alarmId])
        {
            allDeactivated = 0;
            alarmDurations[alarmId]--;
        }
        else
        {
            deactivateAlarm(alarmId);
        }
    }
    
    if (allDeactivated)
    {
        TCCR2 = 0x00;
    }
}

void showError(const uint8_t stage, const uint8_t code)
{
    int i;
    for(i = 0; i < 2; i++)
    {
        ipAddres[i * 6 + 0] = 0x05;
        ipAddres[i * 6 + 1] = stage >> 4;
        ipAddres[i * 6 + 2] = stage & 0x0F;
        
        ipAddres[i * 6 + 3] = 0x0E;
        ipAddres[i * 6 + 4] = code >> 4;
        ipAddres[i * 6 + 5] = code & 0x0F;
    }

    TCCR0 |= (1 << CS01);  // starts timer with clk/8
}

void activateAlarm(const int id)
{
    switch(id)
    {
    case 0:
        break;
    case 1:
        PORTD |= (1 << PORTD4);
        break;
    case 2:
        PORTD |= (1 << PORTD5);
        break;
    case 3:
        PORTD |= (1 << PORTD6);
        break;
    case 4:
        PORTD |= (1 << PORTD7);
        break;
    case 5:
        PORTC |= (1 << PORTC3);
        break;
    default:
        break;
    }
}

void deactivateAlarm(const int id)
{
    switch(id)
    {
    case 0:
        break;
    case 1:
        PORTD &= ~(1 << PORTD4);
        break;
    case 2:
        PORTD &= ~(1 << PORTD5);
        break;
    case 3:
        PORTD &= ~(1 << PORTD6);
        break;
    case 4:
        PORTD &= ~(1 << PORTD7);
        break;
    case 5:
        PORTC &= ~(1 << PORTC3);
        break;
    default:
        break;
    }
}

void enableRtcModule(void)
{
    PORTD |= (1 << PORTD3);
    i2cInit();
}

void disableRtcModule(void)
{
    i2cDeinit();
    PORTD &= ~(1 << PORTD3);
}

void getAlarmTime(
    const int id, 
    uint8_t *pAlarm)
{
    if (MAX_ALARMS_NUMBER < id)
    {
        return;
    }

    enableRtcModule();

    uint8_t errorCode = 0;    
    uint16_t address = AT24C32_ALARM_ADDRESS(id);
    uint8_t i;
    for (i = 0; i < sizeof(alarm_t); i++)
    {
        int res = at24c32ReadByte(address + i, pAlarm + i, &errorCode);
        if(AT24C32_OK != res)
        {
            showError(0x03, errorCode);
            return;
        }
    }
    disableRtcModule();
}

void indicateActiveMode(void)
{
    PORTD |= (1 << PORTD2);
}

void indicateInactiveMode(void)
{
    PORTD &= ~(1 << PORTD2);
}

void checkAlarms(void)
{
    time_t current;
    int res = DS3231_ERROR;
    uint8_t errorCode = 0;
    
    enableRtcModule();
    res = ds3231ReadTime(&current, &errorCode);
    disableRtcModule();
    
    if(DS3231_OK != res)
    {
        showError(0x02, errorCode);
        return;
    }
    
    int alarmId;
    for (alarmId = 0; alarmId < MAX_ALARMS_NUMBER; alarmId++)
    {   
        alarm_t* pAlarm = &alarms[alarmId];
        if ( ! pAlarm->enabled)
        {
            continue;
        }
        
        time_t * pTime = &pAlarm->time;
        uint8_t dateIsEqual  = (TIMEVAL_IS_NOT_INITIALIZED == pTime->day)  || (pTime->day == current.day);
        uint8_t monthIsEqual = (TIMEVAL_IS_NOT_INITIALIZED == pTime->month) || (pTime->month == current.month);
        uint8_t hoursIsEqual = (TIMEVAL_IS_NOT_INITIALIZED == pTime->hours) || (pTime->hours == current.hours);
        uint8_t minutesIsEqual = (pTime->minutes == current.minutes);
        uint8_t timeIsEqual = dateIsEqual && monthIsEqual && hoursIsEqual && minutesIsEqual;

        if (timeIsEqual && ( ! pAlarm->isActivated))
        {
            activateAlarm(alarmId);
            
            alarmDurations[alarmId] = TMR2_OVERFLOWS_PER_SECOND * pAlarm->duration;
            
            TCCR2 = 0x06;   // start TIMER2 on clk / 256 in Normal mode
            pAlarm->isActivated = 1;
        }
        if (( ! timeIsEqual) && pAlarm->isActivated)
        {
            pAlarm->isActivated = 0;
        }
    }
}

void answerOk(void)
{
    answer[0] = ANSWER_OK;
    answer[1] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
}

void answerFail(void)
{
    answer[0] = ANSWER_FAIL;
    answer[1] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
}

void showIp(void)
{
    if (END_OF_COMMAND == command[13])
    {
        int commandCnt = 1;
        int ipAddressCnt = 0;
        for (int i = 0; i < 4; i++)
        {
            int initialZeroes = 1;
            for (int j = 0; j < 3; j++)
            {
                if (0 == command[commandCnt] && initialZeroes && j != 2)
                {
                    ipAddres[ipAddressCnt] = HIDE_ALL_SEGMENTS_IDX;
                }
                else
                {
                    ipAddres[ipAddressCnt] = command[commandCnt];
                    initialZeroes = 0;
                }
                commandCnt++;
                ipAddressCnt++;
            }
        }
        answerOk();

        // show IP
        TCCR0 |= (1 << CS01);  // starts timer with clk / 8
    }
    else
    {
        answerFail();   
    }
}

void getCurrentTime(void) 
{
    time_t current;
    int res = DS3231_ERROR;
    uint8_t errorCode = 0;
    enableRtcModule();
    res = ds3231ReadTime(&current, &errorCode);
    disableRtcModule();
    if(DS3231_OK != res)
    {
        showError(CMD_GET_CURRENT_TIME, errorCode);
    }

    answer[0] = current.day;
    answer[1] = current.month;
    answer[2] = current.year;
    answer[3] = current.hours;
    answer[4] = current.minutes;
    answer[5] = current.seconds;
    answer[6] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
}

void setCurrentTime(void) 
{
    if (END_OF_COMMAND == command[7])
    {
        int res = DS3231_ERROR;
        uint8_t errorCode = 0;
        time_t current;
        current.day = command[1];
        current.month = command[2];
        current.year = command[3];
        current.hours = command[4];
        current.minutes = command[5];
        current.seconds = command[6];
        enableRtcModule();
        res = ds3231WriteTime(&current, &errorCode);
        disableRtcModule();
        if(DS3231_OK != res)
        {
            showError(CMD_SET_CURRENT_TIME, errorCode);
            answerFail();
        }
        else
        {
            answerOk();
        }
    }
    else
    {
        answerFail();
    }
}

void getAlarm(void)
{                
    uint8_t alarmId = command[0] & 0x0F;
    
    if (alarmId < MAX_ALARMS_NUMBER)
    {
        alarm_t* pAlarm = &alarms[alarmId];
        time_t*  pTime  = &pAlarm->time;
        answer[0] = pTime->day;
        answer[1] = pTime->month;
        answer[2] = pTime->hours;
        answer[3] = pTime->minutes;
        answer[4] = pAlarm->duration;
        answer[5] = pAlarm->enabled;
        answer[6] = END_OF_ANSWER;
        answerCnt = 0;
        UCSRB |= (1 << UDRIE);
    }
}

void setAlarm(void)
{
    uint8_t alarmId = command[0] & 0x0F;
    if (alarmId < MAX_ALARMS_NUMBER)
    {
        alarm_t* pAlarm = &alarms[alarmId];
        time_t*  pTime  = &pAlarm->time;
        pTime->day     = command[1];
        pTime->month   = command[2];
        pTime->hours   = command[3];
        pTime->minutes = command[4];
        pAlarm->duration     = command[5];
        pAlarm->enabled      = command[6];
        pAlarm->isActivated  = 0;
        
        // store alarm in the EEPROM
        enableRtcModule();
        uint16_t eeAddress = AT24C32_ALARM_ADDRESS(alarmId);
        uint8_t errorCode;
        uint8_t i;
        for (i = 0; i < sizeof(alarm_t); i++)
        {
            int res = at24c32WriteByte(eeAddress + i, *((uint8_t*)pAlarm + i), &errorCode);
            if(AT24C32_OK != res)
            {
                disableRtcModule();
                showError(CMD_SET_ALARM_TIME, errorCode);
                answerFail();
                return;
            }
        }
        disableRtcModule();
        
        answerOk();
        
        TCNT1 = 0x8000 - 0x0010;    // request alarms check
    }
    else
    {
        answerFail();
    }
}

void getAlarmsStatus(void)
{
    answer[0] = (PORTD >> PORTD4) & 0x01;
    answer[1] = (PORTD >> PORTD5) & 0x01;
    answer[2] = (PORTD >> PORTD6) & 0x01;
    answer[3] = (PORTD >> PORTD7) & 0x01;
    answer[4] = (PORTC >> PORTC3) & 0x01;
    answer[5] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
}

void getTemperature(void)
{
    int8_t intPart = 0;
    uint8_t fracPart = 0;
    int res = DS3231_ERROR;
    uint8_t errorCode = 0;
    enableRtcModule();
    res = ds3231ReadTemperature(&intPart, &fracPart, &errorCode);
    disableRtcModule();
    if(DS3231_OK != res)
    {
        showError(CMD_GET_CURRENT_TIME, errorCode);
    }

    answer[0] = intPart;
    answer[1] = fracPart;
    answer[2] = END_OF_ANSWER;
    answerCnt = 0;
    UCSRB |= (1 << UDRIE);
}
int main(void)
{
    memset(alarms, 0, sizeof(alarms));
    memset(&alarmDurations, 0, sizeof(alarmDurations));

    DDRB = 0xFF;	// PB* are for segments
    DDRC = 0x0F;	// PC0-2 are for choosing digit, PC3 is for the Alarm#5
    PORTC &= 0xF8;
    PORTB = 0x00;
    
    // setup PD2-7 as output
    DDRD |= (1 << DDD2);
    DDRD |= (1 << DDD3);
    DDRD |= (1 << DDD4);
    DDRD |= (1 << DDD5);
    DDRD |= (1 << DDD6);
    DDRD |= (1 << DDD7);
    
    // setup UART 
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART Receiver: On
    // USART Transmitter: On
    // USART Mode: Asynchronous
    // USART Baud Rate: 9600
    // PD0 and PD1 are inputs
    DDRD &= ~(1 << DDD0); 
    PORTD &= ~(1 << PORTD0);
    DDRD &= ~(1 << DDD1);
    PORTD &= ~(1 << PORTD1);
    UCSRA=0x02;
    UCSRB=0x18;
    UCSRC=0x86;
    UBRRH=0x00;
    UBRRL=0x0C;
    UCSRB |= (1 << RXCIE);
    
    TIMSK |= (1 << TOIE0);		// enable TIMER0_OVF interupt
    
    TCCR1A = 0x00;
    TCCR1B = 0x0D;              // start TIMER1 on clk / 1024 in CTC mode
    OCR1A = 0x8000;
    TIMSK |= (1 << OCIE1A);	// enable TIMER1_OVF interupt
    
    TIMSK |= (1 << TOIE2);
    
    indicateActiveMode();
    
    sei();	// turn on interrupts
    
    i2cInit();

    {
        enableRtcModule();
        uint8_t ds3231InitStatus;
        if (DS3231_OK != ds3231Init(&ds3231InitStatus))
        {
            showError(0x01, ds3231InitStatus);
        }
        disableRtcModule();
    }
    
    {
        int alarmId;
        for (alarmId = 0; alarmId < MAX_ALARMS_NUMBER; alarmId++)
        {
            getAlarmTime(alarmId, (uint8_t*)&alarms[alarmId]);
        }   
    }
    
    indicateInactiveMode();
    
    while(1)
    {
        set_sleep_mode(SLEEP_MODE_IDLE);
        sleep_mode();
        
        if (checkOfAlarmsRequired)
        {   
            indicateActiveMode();
            
            checkAlarms();
            
            checkOfAlarmsRequired = 0;
            
            indicateInactiveMode();
        }
        
        if (commandReceived)
        {
            indicateActiveMode();
            
            if (command[0] == CMD_SHOW_IP_ADDRES)
            {
                // show IP - code 0x10
                // command for "192.168.1.4" is {0x10, 1, 9, 2, 1, 6, 8, 0, 0, 1, 0, 0, 4, 0xFF}
                // echo -ne \\x10\\x01\\x09\\x02\\x01\\x06\\x08\\x00\\x00\\x01\\x00\\x00\\x04\\xFF > /dev/ttyATH0
                
                showIp();
            }
            else if (command[0] == CMD_GET_CURRENT_TIME)
            {
                // get time - code 0x30
                // command for reading time is {0x30, 0xFF}
                // echo -ne \\x30\\xFF > /dev/ttyATH0
                
                getCurrentTime();
            }            
            else if (command[0] == CMD_SET_CURRENT_TIME)
            {
                // set time - code 0x31
                // command for storing 28/05/2014 21:59:06 is {0x31, 28, 05, 14, 21, 59, 06, 0xFF}
                
                setCurrentTime();
            }
            else if ((command[0] & 0xF0) == CMD_GET_ALARM_TIME)
            {
                // get alarm time - code 0x5X, where X is an alarm ID (0, 1, 2, 3, 4)
                // echo -ne \\x51\\xFF > /dev/ttyATH0
                
                getAlarm();
            }
            else if ((command[0] & 0xF0) == CMD_SET_ALARM_TIME)
            {
                // set (store in the eeprom) alarm time - code 0x7X, where X is an alarm ID (1, 2, 3, 4 or 5)
                // command for setting 28 may 20:59 on the first alarm with 7 sec duration is {0x71, 28, 05, 20, 59, 07, 0xFF}
                // command for setting 16:23 on the second alarm with 1 sec duration is {0x72, 0xF0, 0xF0, 16, 23, 01, 0x0A}
                
                setAlarm();
            }
            else if ((command[0]) == CMD_GET_ALARMS_STATUS)
            {
                // get alarms status - code 0x80
                // echo -ne \\x80\\xFF > /dev/ttyATH0
                
                getAlarmsStatus();
            }
            else if (command[0] == CMD_GET_TEMPERATURE)
            {
                // get temperature - code 0x90
                
                getTemperature();
            }

            commandReceived = 0;
            memset(command, 0, sizeof(command));
            indicateInactiveMode();
        }
        
    }
}