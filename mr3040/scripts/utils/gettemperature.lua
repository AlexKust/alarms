#!/usr/bin/lua

package.path = package.path .. ";/scripts/?.lua"
local ipc = require "ipc"

s = ipc.open(ipc.IPC_REQ_SOCKET)
local reply = ipc.send_recv(s, ipc.CMD_GET_TEMP)
print(reply)
ipc.close(s)