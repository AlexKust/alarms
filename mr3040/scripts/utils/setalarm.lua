#!/usr/bin/lua 

-- Set alarm 4 on 7 Dec 04:05 for 6 seconds: /scripts/utils/setalarm.lua 4 7 12 4 5 6

package.path = package.path .. ";/scripts/?.lua"
local ipc = require "ipc"

s = ipc.open(ipc.IPC_REQ_SOCKET)
local alarmData = string.format('{"date":%q, "month":%q, "hours":%q, "minutes":%q, "duration":%q, "enabled":true}',
    arg[2], arg[3], arg[4], arg[5], arg[6])
local reply = ipc.send_recv(s, ipc.CMD_SET_ALARM .. " " .. arg[1] .. " " ..  alarmData)
print(reply)
ipc.close(s)