#!/usr/bin/lua

-- set 16/11/2013 7:30:56 : setcurrenttime.lua 16 11 2013 7 30 56
-- set server time: setcurrenttime.lua

package.path = package.path .. ";/scripts/?.lua"
local ipc = require "ipc"

s = ipc.open(ipc.IPC_REQ_SOCKET)
local timeData = ""
if arg[1] then
    timeData = string.format('{"date":%q, "month":%q, "year":%q, "hours":%q, "minutes":%q, "seconds":%q}',
        arg[1], arg[2], arg[3], arg[4], arg[5], arg[6])
end
local reply = ipc.send_recv(s, ipc.CMD_SET_TIME .. " " .. timeData)
print(reply)

ipc.close(s)