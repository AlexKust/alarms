#!/usr/bin/lua

package.path = package.path .. ";/scripts/?.lua"

require "syslog"
local iserial = require "iserial"

syslog.openlog("ippresenter", syslog.LOG_PERROR + syslog.LOG_ODELAY, "LOG_USER")
syslog.syslog("LOG_INFO", "Start")

local function is_ip_display(dev_name)
    local d = iserial.open(dev_name)
    if d then
        local mc_cmd = {iserial.CMD_GET_DEVICE_ID}
        local mc_answ = iserial.send_recv(d, mc_cmd)
        if mc_answ:len() > 1 then
            if iserial.IP_DISPLAY_ID == string.byte(mc_answ, 1) then
                return true
            end
        else
            syslog.syslog("LOG_WARNING", "Can't read from " .. dev_name)
        end
        iserial.close(d)
    else
        syslog.syslog("LOG_WARNING", "Can't open " .. dev_name)
    end

    return false
end

local function get_dev_name()
    local command = "ls /dev | grep USB"
    local handle = io.popen(command)
    local names = handle:read("*a")
    local devices = {}
    for name in names:gmatch("%w+") do
        table.insert(devices, name)
    end
    handle:close()

    local dev_name = ""
    while "" == dev_name do
        for i,name in pairs(devices) do
            local full_name = '/dev/' .. name
            if is_ip_display(full_name) then
                syslog.syslog("LOG_INFO", name .. " is the IP-display")
                dev_name = full_name
            end
        end

        if "" == dev_name then
            os.execute("sleep " .. tonumber(10))
        end
    end

    return dev_name
end

local function get_ip()
    local command = "/scripts/getip.sh"
    local handle = io.popen(command)
    local ip = handle:read("*a")
    ip = string.gsub(ip, '[\n\r]+', '')
    handle:close()

    return ip
end

local function show_ip(port, ip)
    local mc_cmd = {iserial.CMD_SHOW_IP}
    local zeroes_table = {
        [0] = {0, 0, 0},
        [1] = {0, 0},
        [2] = {0},
        [3] = {}
    }
    for ip_byte in ip:gmatch("%d+") do
        -- add zeroes
        table.foreach(zeroes_table[ip_byte:len()],function(i,v)table.insert(mc_cmd,v)end)
        -- add digits
        for ip_digit in ip_byte:gmatch"." do
            table.insert(mc_cmd, tonumber(ip_digit))
        end
    end

    local mc_answ = iserial.send_recv(port, mc_cmd)
    if 0 == mc_answ:len() or 0 ~= string.byte(mc_answ, 1) then
        syslog.syslog("LOG_ERR", "Failed to send IP")
    end
end

dev_name = get_dev_name()

p = iserial.open(dev_name)

while 1 do

    ip = get_ip()

    if ip:len() > 7 then
        syslog.syslog("LOG_INFO", "Show ip " .. ip)
        show_ip(p, ip)
        os.execute("sleep " .. tonumber(10))
    end
end

iserial.close(p)

syslog.syslog("LOG_INFO", "Stop")
syslog.closelog()