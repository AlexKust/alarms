#!/usr/bin/lua

package.path = package.path .. ";/scripts/?.lua"

require "syslog"
local JSON = loadfile("/scripts/JSON.lua")()
local iserial = require "iserial"
local ipc = require "ipc"

syslog.openlog("alarmsrv", syslog.LOG_PERROR + syslog.LOG_ODELAY, "LOG_USER")
syslog.syslog("LOG_INFO", "Start")

local function is_empty(s)
  return s == nil or s == ''
end

local function show_ip(ip)
    syslog.syslog("LOG_INFO", "Show IP " .. ip)
    
    local atmega_cmd = {iserial.CMD_SHOW_IP}
    local zeroes_table = {
        [0] = {0, 0, 0},
        [1] = {0, 0},
        [2] = {0},
        [3] = {}
    }
    for ip_byte in ip:gmatch("%d+") do
        -- add zeroes
        table.foreach(zeroes_table[ip_byte:len()],function(i,v)table.insert(atmega_cmd,v)end)
        -- add digits
        for ip_digit in ip_byte:gmatch"." do
            table.insert(atmega_cmd, tonumber(ip_digit))
		end
    end

    local atmega_answ = iserial.send_recv(p, atmega_cmd)
    if is_empty(atmega_answ) or 0 ~= string.byte(atmega_answ, 1) then
        return ipc.ANSW_FAIL
    end
    
    return ipc.ANSW_OK
end

local function get_time()
    syslog.syslog("LOG_INFO", "Get current time")
	
    local atmega_cmd = {iserial.CMD_GET_CURRENT_TIME}
    local atmega_answ = iserial.send_recv(p, atmega_cmd)
    if is_empty(atmega_answ) then
        return ipc.ANSW_FAIL
    end
    local day = string.byte(atmega_answ, 1)
    local month = string.byte(atmega_answ, 2)
    local year = string.byte(atmega_answ, 3)
    local hours = string.byte(atmega_answ, 4)
    local minutes = string.byte(atmega_answ, 5)
    local seconds = string.byte(atmega_answ, 6)
    local reply = string.format('{"year": %d, "month": %d, "date": %d, "hours": %d, "minutes": %d, "seconds": %d}',
        	year + 2000, month, day, hours, minutes, seconds)
 
    syslog.syslog("LOG_INFO", string.format("Time is %d/%d/%d %d:%d:%d", day, month, year, hours, minutes, seconds))
    return reply
end

local function set_time(args)
    local current_time = os.date("*t", os.time())
    
    if args ~= "" then
        local time = JSON:decode(args)
        current_time.year = tonumber(time["year"])
        current_time.day = tonumber(time["date"])
        current_time.month = tonumber(time["month"])
        current_time.hour = tonumber(time["hours"])
        current_time.min = tonumber(time["minutes"])
        current_time.sec = tonumber(time["seconds"])
    end
    
    syslog.syslog("LOG_INFO", string.format('Set time %d/%d/%d %d:%d:%d', 
        current_time.day, current_time.month, current_time.year, current_time.hour,  current_time.min, current_time.sec))
    local atmega_cmd = {iserial.CMD_SET_CURRENT_TIME, current_time.day, current_time.month, current_time.year - 2000, current_time.hour, current_time.min, current_time.sec}
    local atmega_answ = iserial.send_recv(p, atmega_cmd)
    if is_empty(atmega_answ) or 0 ~= string.byte(atmega_answ, 1) then
        return ipc.ANSW_FAIL
    end

    return ipc.ANSW_OK
end

local function get_alarm(args)
    local id = tonumber(args)
    syslog.syslog("LOG_INFO", "Get alarm " .. id)
    local atmega_cmd = {iserial.CMD_GET_ALARM_TIME + id}
    local atmega_answ = iserial.send_recv(p, atmega_cmd)
    if is_empty(atmega_answ) then
        return ipc.ANSW_FAIL
    end
    local day = string.byte(atmega_answ, 1)
    local month = string.byte(atmega_answ, 2)
    local hours = string.byte(atmega_answ, 3)
    local minutes = string.byte(atmega_answ, 4)
    local duration = string.byte(atmega_answ, 5)
    local enabled = 'true'
    if 0 == string.byte(atmega_answ, 6) then
    	enabled = 'false'
    end
    local reply = string.format('{"enabled": %s, "month": %d, "date": %d, "hours": %d, "minutes": %d, "duration": %d}',
         enabled, month, day, hours, minutes, duration)

    syslog.syslog("LOG_INFO", string.format("Alarm on %d/%d %d:%d for %d", day, month, hours, minutes, duration))
    return reply
end

local function set_alarm(args)
    local id, params = string.match(args, "(%d*)%s*(.*)")
    local alarm = JSON:decode(params)
    
    local day = tonumber(alarm["date"])
    local month = tonumber(alarm["month"])
    local hour = tonumber(alarm["hours"])
    local minute = tonumber(alarm["minutes"])
    local second = 0
    local duration = tonumber(alarm["duration"])
    local enabled = alarm["enabled"] and 1 or 0  -- convert to int
    
    syslog.syslog("LOG_INFO", string.format("Set alarm %d (%d): %d/%d %d:%d for %d", id, enabled, day, month, hour, minute, duration))
   
    local atmega_cmd = {iserial.CMD_SET_ALARM_TIME + id, day, month, hour, minute, duration, enabled}
    local atmega_answ = iserial.send_recv(p, atmega_cmd)
    if is_empty(atmega_answ) or 0 ~= string.byte(atmega_answ, 1) then
        return ipc.ANSW_FAIL
    end

    return ipc.ANSW_OK
end

local function get_temp()
    syslog.syslog("LOG_INFO", "Get temperature")
    local atmega_cmd = {iserial.CMD_GET_TEMPERATURE}
    local atmega_answ = iserial.send_recv(p, atmega_cmd)
    if is_empty(atmega_answ) then
        return ipc.ANSW_FAIL
    end
    local intPart = string.byte(atmega_answ, 1)
    local fracPart = string.byte(atmega_answ, 2)
    syslog.syslog("LOG_INFO", string.format("Temperature is %d.%d", intPart, fracPart))
    local reply = string.format('{"temperature": "%d.%d"}', intPart, fracPart)
    
    return reply
end

local action = {
    [ipc.CMD_SHOW_IP] = function (ip) return show_ip(ip) end,
    [ipc.CMD_GET_TIME] = function () return get_time() end,
    [ipc.CMD_SET_TIME] = function (args) return set_time(args) end,
    [ipc.CMD_GET_ALARM] = function (id) return get_alarm(id) end,
    [ipc.CMD_SET_ALARM] = function (args) return set_alarm(args) end,
    [ipc.CMD_GET_TEMP] = function () return get_temp() end
}

p = iserial.open()
s = ipc.open(ipc.IPC_REP_SOCKET)

while true do
    local request = ipc.recv(s)
    syslog.syslog("LOG_INFO", "Received query: " .. request)
    local cmd, _, args = string.match(request, "(%S+)(%s*)(.*)")
	local reply
	if args then 
		reply = action[cmd](args)
	else
		reply = action[cmd]()
	end
    syslog.syslog("LOG_INFO", "Send: " .. reply)
    ipc.send(s, reply)
end

ipc.close(s)
iserial.close(p)

syslog.syslog("LOG_INFO", "Stop")
syslog.closelog()