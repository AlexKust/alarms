
package.path = package.path .. ";/scripts/?.lua"
local ipc = require "ipc" 

function send_404()
    uhttpd.send("HTTP/1.1 404 Not Found\r\n")
    uhttpd.send("Content-type: text/html\r\n")
    local response = "<html><head><title>Not Found</title></head><body>" ..
        "Sorry, the object you requested was not found." ..
        "</body><html>"
    uhttpd.send("Content-length: " .. tostring(string.len(response)) .. "\r\n\r\n")
    uhttpd.send(response)
end

function send_405(not_allowed_method, allowed_methods)
    uhttpd.send("HTTP/1.1 405 Method Not Allowed\r\n")
    uhttpd.send("Content-type: text/html\r\n")
    local response = "<html><head><title>Method Not Allowed</title></head><body>" ..
        string.format("Sorry, the %q method is not supported. The allowed methods are: %s",
            not_allowed_method, allowed_methods) ..
        "</body><html>"
    uhttpd.send("Content-length: " .. tostring(string.len(response)) .. "\r\n\r\n")
    uhttpd.send(response)
end

function send_500(msg)
    uhttpd.send("HTTP/1.1 500 Internal Server Error\r\n")
    uhttpd.send("Content-type: text/html\r\n")
    uhttpd.send("Content-length: " .. tostring(string.len(msg)) .. "\r\n\r\n")
    uhttpd.send(msg);
end

function send_ok(msg)
    uhttpd.send("HTTP/1.1 200 OK\r\n")
    uhttpd.send("Content-type: text/html\r\n")
    uhttpd.send("Content-length: " .. tostring(string.len(msg)) .. "\r\n\r\n")
    uhttpd.send(msg);
end

function recv_data()
    local rv, buf
    local data = ""
    repeat
        rv, buf = uhttpd.recv(4096)
        if buf then
            data = data..buf
        end
    until rv <= 0
    
    return data
end

function process_alarm_request(method, id)
    s = ipc.open(ipc.IPC_REQ_SOCKET)

    if 'GET' == method then
        local reply = ipc.send_recv(s, ipc.CMD_GET_ALARM .. " " .. id)
        if reply ~= "" then
            uhttpd.send("HTTP/1.1 200 OK\r\n")
            uhttpd.send("Content-Type: application/json\r\n\r\n")
            uhttpd.send(reply)
        else
            send_500("Can't get alarm params")
        end
    elseif 'POST' == method then
    	local data = recv_data()
    	if data ~= "" then
            local reply = ipc.send_recv(s, ipc.CMD_SET_ALARM .. " " .. id .. " " .. data)
            if "Ok" == reply then
    	        send_ok("Alarm set")
    	    else
    	        send_500("Can't set alarm")
    	    end
    	else
            send_500("There is no alarm settings")	
    	end
    else
        send_405(method, "GET and POST")
    end
    
    ipc.close(s)
end

function process_time_request(method)
    s = ipc.open(ipc.IPC_REQ_SOCKET)

    if 'GET' == method then
        local reply = ipc.send_recv(s, ipc.CMD_GET_TIME) 
        if reply ~= "" then
            uhttpd.send("HTTP/1.1 200 OK\r\n")
            uhttpd.send("Content-Type: application/json\r\n\r\n")
            uhttpd.send(reply)
        else
            send_500("Can't get time");        
        end
    elseif 'POST' == method then
        local data = recv_data()
        if data ~= "" then
            local reply = ipc.send_recv(s, ipc.CMD_SET_TIME .. " " .. data) 
            if "Ok" == reply then
                send_ok("Time set")
            else
                send_500("Can't set time")
            end
        else
            send_500("There is no time settings")
        end
    else
    	send_405(method, "GET and POST")
    end
    
    ipc.close(s)
end

function process_temperature_request(method)
    s = ipc.open(ipc.IPC_REQ_SOCKET)

    if 'GET' == method then
        local reply = ipc.send_recv(s, ipc.CMD_GET_TEMP)
        if reply ~= "" then
            uhttpd.send("HTTP/1.1 200 OK\r\n")
            uhttpd.send("Content-Type: application/json\r\n\r\n")
            uhttpd.send(reply)
        else
            send_500("Can't get time");        
        end
    else
    	send_405(method, "GET")
    end
    
    ipc.close(s)
end

function handle_request(env)
    local resource_path = {}
    for rp in string.gmatch (env["PATH_INFO"], "%w+") do
        table.insert(resource_path, rp)
    end
    
    if resource_path[1] == "alarms" then
    	local id = resource_path[2]
    	process_alarm_request(env["REQUEST_METHOD"], id)
    elseif resource_path[1] == "time" then
    	process_time_request(env["REQUEST_METHOD"])
    elseif resource_path[1] == "temperature" then
    	process_temperature_request(env["REQUEST_METHOD"])
    else
    	send_404()
    end
end

