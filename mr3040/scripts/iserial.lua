local iserial = {}

local rs232 = require("rs232")

iserial.timeout = 1000

iserial.IP_DISPLAY_ID = 0x01
iserial.ALARM_ID =      0x02

iserial.CMD_GET_DEVICE_ID = 0x01
iserial.CMD_SHOW_IP = 0x10
iserial.CMD_GET_CURRENT_TIME = 0x30
iserial.CMD_SET_CURRENT_TIME = 0x31
iserial.CMD_GET_ALARM_TIME = 0x50
iserial.CMD_SET_ALARM_TIME = 0x70
iserial.CMD_GET_ALARMS_STATUS = 0x80
iserial.CMD_GET_TEMPERATURE = 0x90
iserial.CMD_END = 0xFF

iserial.ANSW_END = 0xFF
iserial.ANSW_OK = 0x00
iserial.ANSW_FAIL = 0x01

function iserial.open(dev_name)
    p = rs232.new()
    local res
    res = p:open({ dev=dev_name, baud='BAUD_9600'})
    if not res then
        print("Can't open port")
        return nil
    end
    
    return p
end

function iserial.send(p, data)
    table.insert(data, iserial.CMD_END)
    local res, len_or_err = p:write(string.char(unpack(data)), iserial.timeout)
    if not res then
        -- handle error
        print(string.format("RS232: Can't write to port, error: '%s'", rs232.error_tostring(len_or_err)))
        return false
    end
	return true
end

function iserial.recv(p)
    local data_read, err = p:read_until(string.char(iserial.ANSW_END), iserial.timeout)
    if err then
        -- handle error
        print(string.format("RS232: Can't read from port, error: '%s'", err))
        return ""
    end

    return data_read
end

function iserial.send_recv(p, data)
    res = iserial.send(p, data)
    if not res then
        return ""
    end
    
    local atmega_answ = iserial.recv(p)
    if atmega_answ == nil or atmega_answ == '' then
        return ""
    end
    
    return atmega_answ
end

function iserial.close(p)
    p:close()
end

return iserial