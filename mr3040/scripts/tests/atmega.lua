#!/usr/bin/lua

-- Test Atmega8 firmware

package.path = package.path .. ";/scripts/?.lua"
local iserial = require "iserial"

local function send_recv(cmd, answ)
    local atmega_answ = iserial.send_recv(p, cmd)
    if atmega_answ == nil or atmega_answ == '' then
        return false
    end
    
    for k,_ in pairs(answ) do answ[k]=nil end
    for i = 1, atmega_answ:len() do
        table.insert(answ, string.byte(atmega_answ, i))
    end
   
    return true
end

local function test_show_ip(ip, expected_answer)
    for k,v in pairs(ip) do
        io.write(v)
        if math.fmod(k, 3) == 0 and ip[k + 1] then
            io.write(".")
        end 
    end
    io.write("\t") io.flush()

    local atmega_cmd = {iserial.CMD_SHOW_IP}
    table.foreach(ip, function(i,v)table.insert(atmega_cmd,v)end)

    local answ = {}
    assert(send_recv(atmega_cmd, answ), "Send failed")
    assert(table.getn(answ) == 2, "Expected " .. 2 .." answer length but got " .. table.getn(answ) .." instead.")
    assert(answ[1] == expected_answer, "Expected " .. expected_answer .." but got ".. answ[1] .." instead.")
    
    if iserial.ANSW_OK == expected_answer then 
        os.execute("sleep 5")
    end
end

local function test_set_current_time(test_time)
    local answ = {}
    
    local time_tbl = os.date("*t", test_time)
    assert(send_recv({iserial.CMD_SET_CURRENT_TIME, time_tbl.day, time_tbl.month, time_tbl.year - 2000, time_tbl.hour, time_tbl.min, time_tbl.sec}, answ), "Send failed")
    assert(table.getn(answ) == 2, "Expected " .. 2 .." answer length but got " .. table.getn(answ) .." instead.")
    assert(answ[1] == iserial.ANSW_OK, "Error code received: " .. answ[1])
    
    assert(send_recv({iserial.CMD_GET_CURRENT_TIME}, answ), "Send failed")
    local atmega_time = os.time{year = 2000 + answ[3], month = answ[2], day = answ[1], hour = answ[4], min = answ[5], sec = answ[6]}
    local time_diff = math.abs(test_time - atmega_time)
    assert(time_diff < 5, "The time difference is more than 5 seconds: " .. time_diff .. "s")        
end

local function test_current_time()
    local test_time = os.time{year=2001, month=2, day=3, hour=4, min=5, sec=6}
    test_set_current_time(test_time)
    
    local current_time = os.time()
    test_set_current_time(current_time)
  
    return "Ok"
end

local function test_set_alarm(id, day, month, hours, minutes, duration, enabled)
    local answ = {}
    
    assert(send_recv({iserial.CMD_SET_ALARM_TIME + id, day, month, hours, minutes, duration, enabled}, answ), "Send failed")
    assert(table.getn(answ) == 2, "Expected " .. 2 .." answer length but got " .. table.getn(answ) .." instead.")    
    assert(answ[1] == iserial.ANSW_OK, "Error code received: " .. answ[1])
    
    assert(send_recv({iserial.CMD_GET_ALARM_TIME + id}, answ), "Send failed")
    assert(day == answ[1], "Expected " .. day .." day but got ".. answ[1] .." instead.")
    assert(month == answ[2], "Expected " .. month .." month but got ".. answ[2] .." instead.")
    assert(hours == answ[3], "Expected " .. hours .." hours but got ".. answ[3] .." instead.")
    assert(minutes == answ[4], "Expected " .. minutes .." minutes but got ".. answ[4] .." instead.")
    assert(duration == answ[5], "Expected " .. duration .." duration but got ".. answ[5] .." instead.")
    assert(enabled == answ[6], "Expected " .. enabled .." enabled state but got ".. answ[6] .." instead.")
end

local function test_alarm(id, duration)
    local current_time = os.date("*t", os.time())
    local enabled = 1
    local answ = {}
    
    assert(send_recv({iserial.CMD_SET_ALARM_TIME + id, current_time.day, current_time.month, current_time.hour, current_time.min, duration, enabled}, answ), "Send failed")
    assert(table.getn(answ) == 2, "Expected " .. 2 .." answer length but got " .. table.getn(answ) .." instead.")
    assert(answ[1] == iserial.ANSW_OK, "Error code received: " .. answ[1])
    
    assert(send_recv({iserial.CMD_GET_ALARM_TIME + id}, answ), "Send failed")
    assert(current_time.day == answ[1], "Expected " .. current_time.day .." day but got ".. answ[1] .." instead.")
    assert(current_time.month == answ[2], "Expected " .. current_time.month .." month but got ".. answ[2] .." instead.")
    assert(current_time.hour == answ[3], "Expected " .. current_time.hour .." hours but got ".. answ[3] .." instead.")
    assert(current_time.min == answ[4], "Expected " .. current_time.min .." minutes but got ".. answ[4] .." instead.")
    assert(duration == answ[5], "Expected " .. duration .." duration but got ".. answ[5] .." instead.")
    assert(enabled == answ[6], "Expected " .. enabled .." enabled state but got ".. answ[6] .." instead.")
    
    local activated_time = 0
    for s = 1, duration do
        assert(send_recv({iserial.CMD_GET_ALARMS_STATUS}, answ), "Send failed")
        for a = 1, 5 do
            if a == id then
                if answ[a] == 1 then
                    activated_time = activated_time + 1
                end
            else
                assert(answ[a] == 0, "Incorrect status (" .. answ[a] .. ") for " .. a .. " alarm")
            end
        end
        os.execute("sleep 1")
    end
    assert(math.abs(duration - activated_time) < 2, "Expected " .. duration .."s duration but got ".. activated_time .."s instead.")
end

local function test_get_temperature()
    local answ = {}
    assert(send_recv({iserial.CMD_GET_TEMPERATURE}, answ), "Send failed")
    assert(answ[1] > 20, "Temperature is less than 20")
    assert(answ[2] == 0 or answ[2] == 25 or answ[2] == 50 or answ[2] == 75, "Incorrect fractional portion of the T value")
    io.write(string.format("T = %d.%d\t", answ[1], answ[2])) io.flush()
end

local display_tests = {
    "test_ip_normal", 
    "test ip_zeroes1", 
    "test ip_zeroes2", 
    "test ip_short", 
    "test ip_long"}

local alarm_tests = {    
    "test_current_time",
    "test_alarm1",
    "test_alarm2",
    "test_alarm3",
    "test_alarm4",
    "test_alarm5",
    "test_get_temperature"}
    
local tests = {
    ["test_ip_normal"]  = function () test_show_ip({1, 2, 3,  4, 5, 6,  7, 8, 9,  8, 7, 6}, iserial.ANSW_OK) end,
    ["test ip_zeroes1"] = function () test_show_ip({0, 0, 1,  0, 2, 0,  3, 0, 0,  4, 4, 4}, iserial.ANSW_OK) end,
    ["test ip_zeroes2"] = function () test_show_ip({0, 1, 0,  0, 0, 1,  0, 0, 0,  0, 0, 4}, iserial.ANSW_OK) end,
    ["test ip_short"]   = function () test_show_ip({0, 0, 1,  0, 0, 2,  0, 0, 3,  0, 4},    iserial.ANSW_FAIL) end,
    ["test ip_long"]    = function () test_show_ip({0, 0, 1,  0, 0, 2,  0, 0, 3,  0, 0, 4,  5}, iserial.ANSW_FAIL) end,
    ["test_alarm1"]     = function () test_alarm(1, 5) end,
    ["test_alarm2"]     = function () test_alarm(2, 10) end,
    ["test_alarm3"]     = function () test_alarm(3, 7) end,
    ["test_alarm4"]     = function () test_alarm(4, 11) end,
    ["test_alarm5"]     = function () test_alarm(5, 8) end,
    ["test_current_time"]   = function () test_current_time() end,
    ["test_get_temperature"]    = function () test_get_temperature() end
}

p = iserial.open()

local device_id = {}
local test_names = {}
assert(send_recv({iserial.CMD_GET_DEVICE_ID}, device_id), "Send failed")
assert(table.getn(device_id) == 2, "Expected " .. 2 .." answer length but got " .. table.getn(device_id) .." instead.")
if iserial.IP_DISPLAY_ID == device_id[1] then
    print("Test IP-display")
    test_names = display_tests
elseif iserial.ALARM_ID == device_id[1] then
    print("Test AlarmS")
    test_names = alarm_tests
else
    assert(false, "Incorrect device ID: " .. device_id[1] .. table.getn(device_id))
end

for _,name in ipairs(test_names) do
    io.write(name .. "\t") io.flush()
    tests[name]()
    io.write("Ok\n")
end
print("All tests passed")

iserial.close(p)