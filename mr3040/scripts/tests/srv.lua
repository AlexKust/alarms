#!/usr/bin/lua

-- Test alarmsrv.lua

package.path = package.path .. ";/scripts/?.lua"
local ipc = require "ipc"
local JSON = loadfile("/scripts/JSON.lua")()

local function test_ip()
    local ip = "10.9.0.1"
    io.write(ip .. "\t") io.flush()
    local reply = ipc.send_recv(s, ipc.CMD_SHOW_IP .. " ".. ip)
    assert(reply == ipc.ANSW_OK, "Command failed: " .. reply)
end

local function json_to_time(time_json)
    local time = JSON:decode(time_json)
    return os.time{
        year = tonumber(time["year"]), 
        month = tonumber(time["month"]), 
        day = tonumber(time["date"]), 
        hour = tonumber(time["hours"]), 
        min = tonumber(time["minutes"]), 
        sec = tonumber(time["seconds"])
    }
end

local function test_current_time()
    local reply = ipc.send_recv(s, ipc.CMD_SET_TIME)
    assert(reply == ipc.ANSW_OK, "Command failed: " .. reply)
    
    reply = ipc.send_recv(s, ipc.CMD_GET_TIME)
    local current_time = os.time()
    local reply_time = json_to_time(reply)
    local time_diff = math.abs(current_time - reply_time)
    assert(time_diff < 2, "The time difference is more than 2 seconds: " .. time_diff .. "s")     

    os.execute("sleep 5")
    
    current_time = os.time()
    local time_tbl = os.date("*t", current_time)
    local time_str = string.format('{"date":%q, "month":%q, "year":%q, "hours":%q, "minutes":%q, "seconds":%q}',
        time_tbl.day, time_tbl.month, time_tbl.year, time_tbl.hour, time_tbl.min, time_tbl.sec)
    reply = ipc.send_recv(s, ipc.CMD_SET_TIME .. " " .. time_str)
    assert(reply == ipc.ANSW_OK, "Command failed: " .. reply)
    
    reply = ipc.send_recv(s, ipc.CMD_GET_TIME)
    current_time = os.time()
    reply_time = json_to_time(reply)
    time_diff = math.abs(current_time - reply_time)
    assert(time_diff < 2, "The time difference is more than 2 seconds: " .. time_diff .. "s")     
end

local function test_alarm()
    local duration = 5
    local current_time = os.time()
    local time_tbl = os.date("*t", current_time)
    local alarm_data = string.format('{"date":%q, "month":%q, "hours":%q, "minutes":%q, "duration":%q, "enabled":true}',
        time_tbl.day, time_tbl.month, time_tbl.hour, time_tbl.min, duration)
    local reply = ipc.send_recv(s, ipc.CMD_SET_ALARM .. " 1 " .. alarm_data)
    assert(reply == ipc.ANSW_OK, "Command failed: " .. reply)
    
    reply = ipc.send_recv(s, ipc.CMD_GET_ALARM .. " 1")
    local alarm_data = JSON:decode(reply)
    local alarm_day = tonumber(alarm_data["date"])
    local alarm_month = tonumber(alarm_data["month"])
    local alarm_hour = tonumber(alarm_data["hours"])
    local alarm_min = tonumber(alarm_data["minutes"])
    local alarm_duration = tonumber(alarm_data["duration"])
    
    assert(time_tbl.day == alarm_day, "Expected " .. time_tbl.day .." day but got ".. alarm_day .." instead.")
    assert(time_tbl.month == alarm_month, "Expected " .. time_tbl.month .." month but got ".. alarm_month .." instead.")
    assert(time_tbl.hour == alarm_hour, "Expected " .. time_tbl.hour .." hour but got ".. alarm_hour .." instead.")
    assert(time_tbl.min == alarm_min, "Expected " .. time_tbl.min .." minute but got ".. alarm_min .." instead.")
    assert(duration == alarm_duration, "Expected " .. duration .." duration but got ".. alarm_duration .." instead.")
end

local function test_temperature()
    local reply = ipc.send_recv(s, ipc.CMD_GET_TEMP)
    local temperature_data = JSON:decode(reply)
    local temperature_value = tonumber(temperature_data["temperature"])
    io.write(temperature_value .. "\t") io.flush()
    assert(20 < temperature_value or temperature_value < 30, "Incorrect temperature " .. temperature_value)
end

local test_names = {
    "test_ip", 
    "test_current_time",
    "test_alarm",
    "test_temperature"
}

local tests = {
    ["test_ip"]     = function () test_ip() end,
    ["test_current_time"]   = function () test_current_time() end,
    ["test_alarm"]  = function () test_alarm() end,
    ["test_temperature"]    = function () test_temperature() end
}

s = ipc.open(ipc.IPC_REQ_SOCKET)

for _,name in ipairs(test_names) do
    io.write(name .. "\t") io.flush()
    tests[name]()
    io.write("Ok\n")
end
print("All tests passed")

ipc.close(s)
