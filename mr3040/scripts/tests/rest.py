#!/usr/bin/env python

from copy import deepcopy
import datetime
import urllib2
import json
import sys

def test_time(url, test_time):
    test_time_dt = datetime.datetime(
        year = test_time["year"],
        month = test_time["month"],
        day = test_time["date"],
        hour = test_time["hours"],
        minute = test_time["minutes"],
        second = test_time["seconds"]
    )
    sys.stdout.write("Test time {} ".format(test_time_dt))
    sys.stdout.flush()

    req = urllib2.Request(url, json.dumps(test_time), {'Content-Type': 'application/json'})
    resp = urllib2.urlopen(req)
    content = resp.read()
    assert (content == "Time set"), "Request failed"

    page = urllib2.urlopen(url)
    content = page.read()
    srv_time = json.loads(content)
    srv_time_dt = datetime.datetime(
        year = srv_time["year"],
        month = srv_time["month"],
        day = srv_time["date"],
        hour = srv_time["hours"],
        minute = srv_time["minutes"],
        second = srv_time["seconds"]
    )

    time_diff = (srv_time_dt - test_time_dt).total_seconds()
    assert (time_diff < 1),"The time diff is too big: {}!".format(time_diff)

    sys.stdout.write("Ok\n")
    sys.stdout.flush()

def test_alarm(url, test_alarm):
    sys.stdout.write("Test alarm {} ".format(test_alarm))
    sys.stdout.flush()

    req = urllib2.Request(url, json.dumps(test_alarm), {'Content-Type': 'application/json'})
    resp = urllib2.urlopen(req)
    content = resp.read()
    assert (content == "Alarm set"), "Request failed"

    page = urllib2.urlopen(url)
    content = page.read()
    srv_alarm = json.loads(content)
    assert (srv_alarm == test_alarm), "Expected {} alarm but got {} instead.".format(test_alarm, srv_alarm)

    sys.stdout.write("Ok\n")
    sys.stdout.flush()

def test_temperature(url):
    sys.stdout.write("Test temperature ")
    sys.stdout.flush()

    page = urllib2.urlopen(url)
    content = page.read()
    temperature = float(json.loads(content)["temperature"])
    assert (temperature > 20.0), "Temprature is too low: {}".format(temperature)
    assert (temperature < 30.0), "Temprature is too high: {}".format(temperature)

    sys.stdout.write("{} Ok\n".format(temperature))
    sys.stdout.flush()

def main():
    srv_ip = sys.argv[1]
    time_url = "http://{}/lua/time".format(srv_ip)
    alarm_id = 1
    alarm_url = "http://{}/lua/alarms/{}".format(srv_ip, alarm_id)
    temperature_url = "http://{}/lua/temperature".format(srv_ip)

    test_time(time_url, {"year": 2015, "month": 1, "date": 2, "hours": 3, "minutes": 4, "seconds": 5})

    curr_time = datetime.datetime.now();
    test_time(time_url, {
        "year": curr_time.year,
        "month": curr_time.month,
        "date": curr_time.day,
        "hours": curr_time.hour,
        "minutes": curr_time.minute,
        "seconds": curr_time.second
    })

    test_alarm(alarm_url, {"month": 1, "date": 2, "hours": 3, "minutes": 4, "duration": 5, "enabled": True})

    test_temperature(temperature_url)

    print("All tests passed")

    return 0

if __name__ == '__main__':
    main()