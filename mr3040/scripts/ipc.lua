local ipc = {}
 
local zmq = require"zmq"

ipc.IPC_REQ_SOCKET = zmq.REQ
ipc.IPC_REP_SOCKET = zmq.REP

ipc.CMD_SHOW_IP = "ip"
ipc.CMD_GET_TIME = "gettime"
ipc.CMD_SET_TIME = "settime"
ipc.CMD_GET_ALARM = "getalarm"
ipc.CMD_SET_ALARM = "setalarm"
ipc.CMD_GET_TEMP = "gettemp"

ipc.ANSW_OK = "Ok"
ipc.ANSW_FAIL = "Fail"

function ipc.open(tp)
    ctx = zmq.init()
    s = ctx:socket(tp)
    if tp == ipc.IPC_REP_SOCKET then
        s:bind("ipc:///tmp/alrmsrv.ipc")
    end
    if tp == ipc.IPC_REQ_SOCKET then
        s:connect("ipc:///tmp/alrmsrv.ipc")
    end
    return s
end

function ipc.recv(s, timeout_cnt)
    if not timeout_cnt then
        return s:recv()
    end
    
    local msg, err = s:recv(zmq.NOBLOCK)
    if msg then
        return msg
    else
        if timeout_cnt > 0 then
            return ipc.recv(s, timeout_cnt - 1)
        else
            return "Fail";
        end
    end
end

function ipc.send(s, data)
    s:send(data)
end

function ipc.send_recv(s, data)
    ipc.send(s, data)
    return ipc.recv(s, 2048)
end

function ipc.close(s)
    s:close()
    ctx:term()
end

return ipc